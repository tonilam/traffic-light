 TRAFFIC LIGHT
 
 
 In this task you will create a simple simulation using the PyGame
 package.  The program will simulate a stream of cars passing a
 traffic light.  Each time the user clicks the mouse in the window
 the traffic light should change colour in the usual green-yellow-
 red-green cycle.  When the light is red the cars should stop
 once they reach it or a stationary car in front.  Otherwise cars
 should keep entering the flow at one end of the screen and
 exiting at the other.  More details, including a tutorial on
 PyGame programming, can be found in the instruction sheet.
